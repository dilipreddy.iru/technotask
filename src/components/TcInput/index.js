/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import {
  ItemView,
} from './styled';
import { Input } from 'native-base';

class TcInput extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    let {
      style,
      error,
      value,
      onChangeText,
      placeholder,
      placeholderTextColor,
      keyboardType,
    } = this.props;

    return (
      <ItemView  regular error={error} style={style}>
        <Input
          placeholder={placeholder}
          keyboardType={keyboardType}
          placeholderTextColor={placeholderTextColor}
          value={value}
          onChangeText={(val) => {
            onChangeText(val);
            //this.isValid(value);
          }}
          style={style}
        />

      </ItemView>
    );
  }
}

export default TcInput;
