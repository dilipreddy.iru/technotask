/* eslint-disable prettier/prettier */
import styled from 'styled-components/native';
import {Item} from 'native-base';


export const ItemView = styled(Item)`
  border-bottom-width: 1;
  border-top-width: 1;
  border-left-width: 1;
  border-right-width: 1;
  /* border-color: #3D71FF; */
  border-color: ${props => (props.error ? 'red' : '#3D71FF')};
  border-radius: 10px;
  margin-bottom: 20px;
`;
