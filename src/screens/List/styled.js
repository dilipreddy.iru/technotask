/* eslint-disable prettier/prettier */
import styled from 'styled-components/native';
import { ListItem, Icon, Button, Right } from 'native-base';


export const IconWrapperView = styled.View`
  flex: 1;
  flex-direction: row;
  justify-content: space-around;
`;
export const IconWrapperViewSub = styled.View`
  flex-direction: row;
`;

export const RightView = styled(Right)`
  flex: 1;
`;

export const ActionButton = styled(Button)`
    margin: 10px;
    width: 50px;
    text-align: center;
    align-items: center;
    justify-content: center;
    background-color: green;
`;
export const ActionButtonDelete = styled(ActionButton)`
   
    background-color: red;
`;
