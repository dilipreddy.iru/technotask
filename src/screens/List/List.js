/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
// import {View } from 'react-native'
import axios from 'axios';

import {
    Container,
    Header,
    Content,
    List,
    ListItem,
    Text,
    Left,
    Body,
    Icon,
    Thumbnail
} from 'native-base';
import { IconWrapperView, IconWrapperViewSub, RightView, ActionButton, ActionButtonDelete } from './styled.js';
export default class EmployeeList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listData: [],

        };

        this.onDelete = this.onDelete.bind(this);
    }
    async componentDidMount() {
        axios.get('https://www.hatchways.io/api/assessment/students').then((response) => {
            this.setState({
                listData: response.data.students,
            });
        });
    }

    onDelete = (index) => {
        this.setState({
            listData: this.state.listData.filter((_, i) => i !== index)
        });

    };

    onSelect = (index) => {
        const selectedItem = this.state.listData[index];
        this.props.navigation.navigate('Details', { details: selectedItem });
    }

    render() {
        return (
            <Container>
                <Content>
                    {/* <Header style={{backgroundColor: 'none'}} /> */}
                    <Header style={{ alignItems: 'center', color: 'white' }}>
                        <Text style={{ color: 'white', fontSize: 20 }}>List</Text>
                    </Header>
                    {this.state.listData &&
                        this.state.listData.map((item, index) => {
                            return (
                                <List>
                                    <ListItem style={{ backgroundColor: '#5CFFF4' }} avatar noIndent noBorder>
                                        <Left>
                                            <Thumbnail source={{ uri: item.pic }} />
                                        </Left>
                                        <Body>
                                            {item.firstName !== '' && <Text>{item.firstName}</Text>}
                                            {item.company !== '' && <Text>{item.company}</Text>}
                                        </Body>
                                        <RightView>
                                            <IconWrapperView>
                                                <IconWrapperViewSub>
                                                    <ActionButton
                                                        rounded
                                                        onPress={() => this.onSelect(index)}
                                                    >
                                                        <Icon
                                                            style={{ fontSize: 18 }}
                                                            name="check"
                                                            type="FontAwesome"
                                                        />
                                                    </ActionButton>
                                                </IconWrapperViewSub>
                                                <IconWrapperViewSub>
                                                    <ActionButtonDelete
                                                        rounded
                                                        onPress={() => this.onDelete(index)}
                                                    >
                                                        {/* <Text>Register</Text> */}
                                                        <Icon
                                                            style={{ fontSize: 18 }}
                                                            name="delete"
                                                            type="MaterialCommunityIcons"
                                                        />
                                                    </ActionButtonDelete>
                                                </IconWrapperViewSub>
                                            </IconWrapperView>
                                        </RightView>
                                    </ListItem>
                                </List>
                            );
                        })}
                </Content>
            </Container>
        );
    }
}
