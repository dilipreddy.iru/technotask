/* eslint-disable prettier/prettier */
import React, { useEffect, useState } from 'react';
import { StyleSheet } from 'react-native';
import {
    Container,
    Right,
    Content,
    Card,
    CardItem,
    Text,
    Button,
    Icon,
    Left,
    Body,
    View,
    Thumbnail,
} from 'native-base';

import {
    HeaderView,
    HeaderViewText,
  
} from './styled';



export default function Listing(props) {

    const [details, setDetails] = useState([]);

    useEffect(() => {
        async function fetchMyAPI() {
            let name = await props.navigation.getParam('details');

            setDetails(name);
        }

        fetchMyAPI();

    }, [ props.navigation]);

    return (
        <Container>
            <HeaderView>
                <Left>
                    <Button transparent onPress={() => props.navigation.push('List')}>
                        <Icon name="arrow-back" />
                    </Button>
                </Left>
                <Body>
                    <HeaderViewText>Details</HeaderViewText>
                </Body>

                <Right>


                </Right>
            </HeaderView>
            <Content>

                <View style={{ padding: 10 }}>
                    <Card >
                        <CardItem>
                            <Left style={{ padding: 20 }}>
                                <Thumbnail source={{ uri: details.pic }} />
                                <Body>
                                    <Text>{details.firstName}</Text>
                                    <Text note>{details.email}</Text>
                                </Body>
                            </Left>
                        </CardItem>

                        <CardItem>
                            <Left>

                                <Body>
                                    <Text >{details.email}</Text>
                                </Body>
                            </Left>
                        </CardItem>
                        <CardItem>
                            <Left>
                                <Text style={{ textAlign: 'right' }}>{details.company}</Text>
                                <Body>
                                    <Text style={{ textAlign: 'right' }}>{details.city}</Text>
                                </Body>
                            </Left>
                        </CardItem>



                    </Card>
                </View>







            </Content>
        </Container>
    );
}

const styles = StyleSheet.create({
    inputs: {
        borderColor: '#b6cbff',
        borderWidth: 1,
        borderRadius: 10,
    },
    row: {
        flex: 1,
        flexDirection: 'row',
        marginVertical: 20,
    },
    inputWrap: {
        flex: 1,
        marginBottom: 0,
    },
});
