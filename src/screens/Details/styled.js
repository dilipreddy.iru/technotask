/* eslint-disable prettier/prettier */
import styled from 'styled-components/native';
import { Text, View, Container, Button, Header } from 'native-base';



export const ContainerView = styled(Container)`
    justify-content: center;
    height: 30%;
    width: 100%;
    background-color: #ffffff;
`;


export const HeaderViewText = styled(Text)`
  color: white;
  font-size: 20px;
 
`;


export const HeaderView = styled(Header)`
  align-items: center;
`;
