/* eslint-disable prettier/prettier */
import React, { useState } from 'react';
import { TouchableOpacity, StyleSheet } from 'react-native';
import { Text } from 'native-base';
import TcInput from '../../components/TcInput';

import {
  ButtonView,
  LoginButton,
  ContainerView,
  MainView,
  ItemView,
} from './styled';

const userName = 'admin';
const Password = 'test';
export default function Login(props) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [emailCheck, setEmailCheck] = useState(false);
  const [passwordCheck, setPasswordCheck] = useState(false);

  const onPress = () => {

    if (email === '') {
      setEmailCheck(true);
      setPasswordCheck(false);
      // alert('Please fill all the fields');

    }
    else if (password === '') {
      setEmailCheck(false);
      setPasswordCheck(true);
    }
    else if (userName === email && Password === password) {
      setEmailCheck(false);
      setPasswordCheck(false);
       props.navigation.push('List');
    }

    else {
      setEmailCheck(false);
      setPasswordCheck(false);
      alert("email or password wrong");
    }
  };

  return (
    <ContainerView>
      <MainView>
        <ItemView>
          <TcInput
            placeholder="Username"
            error={emailCheck}
            value={email}
            style={{ color: 'black' }}
            onChangeText={(val) => {
              setEmail(val);
            }}
          />
        </ItemView>

        <ItemView>
          <TcInput
            placeholder="PassWord"
            error={passwordCheck}
            value={password}
            style={{ color: 'black' }}
            onChangeText={(val) => {
              setPassword(val);
            }}
          />
        </ItemView>
        <ButtonView>
          <TouchableOpacity>
            <LoginButton
              // rounded
              onPress={onPress}
            >
              <Text>Login</Text>
            </LoginButton>
          </TouchableOpacity>
        </ButtonView>
      </MainView>
    </ContainerView>
  );
}

