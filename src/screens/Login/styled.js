/* eslint-disable prettier/prettier */
import styled from 'styled-components/native';
import { Item, View, Container, Button } from 'native-base';



export const ButtonView = styled(View)`
      width: 100%;
      height: 30%;
      justify-content: center;
      align-items: center;
`;

export const LoginButton = styled(Button)`
      justify-content: center;
      align-items: center;
      margin: 10px;
      text-align: center;
      background-color: #ff8896;
`;
export const ContainerView = styled(Container)`
    justify-content: center;
    height: 0%;
    width: 100%;
    background-color: #ffffff;
`;

export const MainView = styled(View)`
    
    background-color: #ffffff;
`;

export const ItemView = styled(View)`
    
   margin: 10px;
`;


